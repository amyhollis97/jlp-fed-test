import "../styles/globals.scss";

import Layout from "../components/layout/layout";

function MyApp({ Component, pageProps }) {
  return (
    <Layout type="netherworld">
      <Component {...pageProps} />
    </Layout>
  );
}

export default MyApp;
