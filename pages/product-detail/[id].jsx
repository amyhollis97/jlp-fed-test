import ProductCarousel from "../../components/product-carousel/product-carousel";
import "../index.module.scss";
import axios from "axios";

// export async function getServerSideProps(context) {
//   const id = context.params.id;
//   const response = await fetch(
//     "https://api.johnlewis.com/mobile-apps/api/v1/products/" + id
//   );
//   const data = await response.json();

//   return {
//     props: { data },
//   };
// }



const ProductDetail = ({ data }) => {
  return (
    <div>
      <h1><div dangerouslySetInnerHTML={{ __html: data.title }}></div></h1>
      <div style={{width:"100%"}}>
        <div style={{width:"50%", float:"left"}}>
          <ProductCarousel image={data.media.images.urls[0]} alt={data.name} />
        </div>

        <div style={{width:"50%", float:"right", position:"absolute", right:"0", top:"30%"}}>
          <h3>
            <h1>£{data.price.now}</h1>
            <div>{data.displaySpecialOffer}</div>
            <div>{data.additionalServices.includedServices}</div>
          </h3>
        </div>
      </div>
      <div style={{width: "100%", position:"absolute", top:"74%", left:"15%"}}>
        <div>
          <h3>Product information</h3>
        </div>
        <h3>Product specification</h3>
        <ul>
          {data.details.features[0].attributes.map((item) => (
            <li>
              <div><div dangerouslySetInnerHTML={{ __html: item.name }} /></div>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};
export async function getServerSideProps(context){
  try {
    const id = context.params.id;
    const response = await axios.get(
        "https://api.johnlewis.com/mobile-apps/api/v1/products/" + id
    );
    const data = response?.data || {};{ props: { data } };
    return { props: { data } };
  } catch (err){
    console.log(err);
  }
}
export default ProductDetail;
