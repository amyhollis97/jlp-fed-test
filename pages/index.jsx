import Head from "next/head";
import Link from "next/link";
import "./index.module.scss";
import axios from "axios";


  const Home = ({data}) => {
    let data_returned = data.products;
    // limit items to only look at the first 20 items returned from data.products
    let items = data_returned.slice(0,20);
    return (
        <div>
            <link rel="stylesheet" href="index.module.scss"/>
          <Head>
            <title>JL &amp; Partners | Home </title>
            <meta name="keywords" content="shopping"/>
          </Head>
          <div>
            <h1>Dishwashers</h1>
            <div className="content">
              {items.map((item) => (
                  <Link
                      key={item.productId}
                      href={{
                        pathname: "/product-detail/[id]",
                        query: {id: item.productId},
                      }}
                  >
                    <a className="link">
                      <div className="content" style={{width: "24%", float:"right", color:"black", border:"gray 1px solid", height:"495px", overflow:"hidden"}}>
                        <div>
                          <img src={item.image} alt={item.name} style={{maxWidth: "350px", height: "391px", marginLeft: "15%"}}/>
                        </div>
                        <div>{item.title}</div>
                        <div className="price">{item.variantPriceRange.display.max}</div>
                      </div>
                    </a>
                  </Link>
              ))}
            </div>
          </div>
        </div>
    );
  };
export async function getServerSideProps(){
  try {
    const response = await axios.get(
        "https://api.johnlewis.com/search/api/rest/v2/catalog/products/search/keyword?q=dishwasher&key=AIzaSyDD_6O5gUgC4tRW5f9kxC0_76XRC8W7_mI"
    );
    const data = response?.data || {};
    return { props: { data } };
  } catch (err){
    console.log(err);
  }
}
export default Home;
