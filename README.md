# UI Dev Technical Test - Dishwasher App

## Brief

The task was to create a simple webpage with a grid of products and then when one is clicked on, redirect you to a product detail page. 

Prior to this task I had not used or seen NodeJs. If this task could be done in any language/with any framework I would have used a more basic HTML, CSS and Javascript approach. I would have used jQuery with the JavaScript as this speeds up production time and provides ajax functionality that could make the API call and handle any errors for the user. In addition, I would have created a base.html file to put the basic structure of the page in and link to the CSS and JavaScript files. In order to best recreate the desired design I would have used Bootstrap as the helper classes do a lot of the heavy lifting as the CSS classes are already written, and just need to be applied and help with mobile first development and would make the site reactive across different screen sizes. 

In order to carry out this task I needed to teach myself some basics of NodeJS I started off with a NodeJS [Crash Course](https://www.youtube.com/watch?v=fBNz5xF-Kx4). After understanding some basic principles of this environment I set up the project (please note it runs on port 3001 not 3000 as per the original instructions as 3001 is specified in the package.json file).

## Production

The product grid was the first page I tackled as this is the landing page for the site. Upon loading the page I was immediately faced with errors about data not being defined. The first port of call here was to get the API call to return data so that I could see what was already rendering to the page. This led me to learning more about nextjs and used [this](https://nextjs.org/docs/basic-features/data-fetching/get-server-side-props) article to understand the getServerSideProps function. I then came across axios (which I used as I found this easier to get the data from the API, compared to async await fetch function) and managed to get this to return the data being returned from the API. While trying to get the API call to return data I added a try/catch block so that I could see if there was anything wrong with the data coming back, this way it would either return the data or console.log() an error for me to furhter investigate.

This part of the task required only the first 20 items from the API to be displayed. In order to achieve this I sliced the data returned to 20 items as the code loops over all the entities returned to render the grid. In addition, there was no `price.now` in the data returned so this was not rendering correctly. Instead of price, I opted to use `variantPriceRange.display.max`. The use of the word 'now' in the original code implied that some of the dishwashers might have been on sale, so to double check I was going to render the correct price I did an if statement in a for loop to check. So for each item in data.products, check if variantPriceRange.display.max was equal to variantPriceRange.display.min, and console.log either "sale" or "no sale", this allowed me to quickly check every product to see if it was on sale, none of which were.  

In order to best recreate the desired design, I had to use inline styling. Ideally I would have added classes and then put the styling for the class in the scss file. However, I could not get this to pull through to the browser where I was viewing my code. I tried adding a file watcher to compile the scss into a css file (in the same way LESS does), but I could not get this to work, so resorted to inline styling to start making headway with re-creating the designs provided.

## Testing

In my current role I am just starting to learn unit testing, however this is for Python, not Javascript/ NodeJS. 